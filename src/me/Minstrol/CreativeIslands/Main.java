package me.Minstrol.CreativeIslands;

import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.world.WorldSaveEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Main extends JavaPlugin implements Listener {

    Inventory playerSettings;
    Inventory islandSettings;

    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(this, this);
        getConfig().options().copyDefaults(true);
        saveConfig();
        getServer().createWorld(new WorldCreator("CreativeIsland"));
    }

    @Override
    public void onDisable() {
        saveConfig();
    }

    @EventHandler
    public void playerJoin(PlayerJoinEvent event){
        final Player player = event.getPlayer();
        if (getConfig().getString("Users." + player.getUniqueId().toString()) != null){
            if (!(player.getWorld().getName().equals(player.getUniqueId().toString()))){
                getServer().createWorld(new WorldCreator(player.getUniqueId().toString()));
                player.teleport(new Location(Bukkit.getWorld(player.getUniqueId().toString()), 0.5, 150, 0.5));
                player.setGameMode(GameMode.CREATIVE);
            }
            saveConfig();
            giveMenu(player, 35);
            player.setGameMode(GameMode.CREATIVE);
        } else {
            getServer().createWorld(new WorldCreator("WaitingLobby"));
            player.teleport(new Location(Bukkit.getWorld("WaitingLobby"), 0.5, 150, 0.5));
            player.setGameMode(GameMode.ADVENTURE);
            player.sendMessage(ChatColor.YELLOW + "Your island is currently being set up for you, it should take around 15 seconds...");
            World source = Bukkit.getWorld("CreativeIsland");
            File sourceFolder = source.getWorldFolder();
            final File targetFolder = new File(Bukkit.getWorldContainer(), player.getUniqueId().toString());
            final World target = Bukkit.getWorld(player.getUniqueId().toString());
            copyWorld(sourceFolder, targetFolder, player);
            getServer().createWorld(new WorldCreator(player.getUniqueId().toString()));
            this.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
                @Override
                public void run() {
                    if (Bukkit.getWorld(player.getUniqueId().toString()) != null) {
                        player.sendMessage(ChatColor.GREEN + "Your creative island have been created!");
                        player.teleport(new Location(Bukkit.getWorld(player.getUniqueId().toString()), 0.5, 150, 0.5));
                        player.setGameMode(GameMode.CREATIVE);
                    } else {
                        Random rn = new Random();
                        int number = rn.nextInt(9999) + 100;
                        player.sendMessage(ChatColor.RED + "There was an ERROR, please screenshot this whole message and send it to a Admin! Code:" + ChatColor.DARK_GRAY + number);
                    }
                }
            }, 300L);
            giveMenu(player, 4);
            getConfig().set("Users." + player.getUniqueId().toString(), null);
            if (getConfig().get("Users." + player.getUniqueId().toString()) != null) {
                getConfig().set("Users." + player.getUniqueId().toString() + ".Settings.Players.InviteNotifications", Boolean.valueOf(true));
                getConfig().set("Users." + player.getUniqueId().toString() + ".Settings.Island.Visitable", Boolean.valueOf(true));
                saveConfig();
            }
        }
    }

    public void copyWorld(File source, File target, Player player){
        try {
            ArrayList<String> ignore = new ArrayList<String>(Arrays.asList("uid.dat", "session.dat"));
            if(!ignore.contains(source.getName())) {
                if(source.isDirectory()) {
                    if(!target.exists())
                        target.mkdirs();
                    String files[] = source.list();
                    for (String file : files) {
                        File srcFile = new File(source, file);
                        File destFile = new File(target, file);
                        copyWorld(srcFile, destFile, player);
                    }
                    File session = new File(Bukkit.getWorldContainer(), player.getUniqueId().toString() + "/session.lock");
                    session.delete();
                } else {
                    InputStream in = new FileInputStream(source);
                    OutputStream out = new FileOutputStream(target);
                    byte[] buffer = new byte[1024];
                    int length;
                    while ((length = in.read(buffer)) > 0)
                        out.write(buffer, 0, length);
                    in.close();
                    out.close();
                }
            }
        } catch (IOException e) {

        }
    }

    @EventHandler
    public void world(WorldSaveEvent event){
        getServer().createWorld(new WorldCreator(event.getWorld().getName()));
    }

    public void giveMenu(Player player,int slot){
        ItemStack is = new ItemStack(Material.NETHER_STAR, 1);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(ChatColor.GOLD + "Options Menu");
        im.addEnchant(Enchantment.DURABILITY, 1, true);
        im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        im.setLore(Arrays.asList(ChatColor.DARK_GRAY + "Click to open the options", "menu containing all you need", "todo with your island!"));
        is.setItemMeta(im);
        player.getInventory().setItem(slot, is);
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent event) {
        if (event.getInventory() != null) {
        }
        if (event.getInventory() != event.getWhoClicked().getInventory()) {
            event.setCancelled(true);
        }
        if (event.getInventory() != null) {
            if (event.getInventory().getName().equals(ChatColor.DARK_GRAY + "Main menu")) {
                if (event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.AQUA + "Settings")) {
                    openSettingsMenu((Player) event.getWhoClicked());
                }
            }
            if (event.getInventory().getName().equals(ChatColor.DARK_GRAY + "Settings")) {
                if (event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.AQUA + "Your Settings")) {
                    openPlayerSettingsMenu((Player) event.getWhoClicked());
                }
                if (event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.AQUA + "Island Settings")) {
                    openIslandSettingsMenu((Player) event.getWhoClicked());
                }
            }
        }

        if (event.getInventory() != null) {
            if (event.getInventory().getName().equals(ChatColor.DARK_GRAY + event.getWhoClicked().getName() + "'s Settings")) {
                if (event.getCurrentItem().getType().equals(Material.STAINED_CLAY)) {
                    if (event.getInventory().getItem(19).getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Enabled")) {
                        createItem(playerSettings, 19, Material.STAINED_CLAY, 14, ChatColor.RED + "Disabled", new String[]{ChatColor.DARK_GRAY + "Click to enable!"});
                        Bukkit.getWorld(event.getWhoClicked().getWorld().getName()).playSound(event.getWhoClicked().getLocation(), Sound.LEVEL_UP, 5, 5);
                        getConfig().set("Users." + event.getWhoClicked().getUniqueId().toString() + ".Settings.Players.InviteNotifications", Boolean.valueOf(false));
                        saveConfig();
                    } else if (event.getInventory().getItem(19).getItemMeta().getDisplayName().equals(ChatColor.RED + "Disabled")) {
                        createItem(playerSettings, 19, Material.STAINED_CLAY, 5, ChatColor.GREEN + "Enabled", new String[]{ChatColor.DARK_GRAY + "Click to disable!"});
                        Bukkit.getWorld(event.getWhoClicked().getWorld().getName()).playSound(event.getWhoClicked().getLocation(), Sound.LEVEL_UP, 5, 5);
                        getConfig().set("Users." + event.getWhoClicked().getUniqueId().toString() + ".Settings.Players.InviteNotifications", Boolean.valueOf(true));
                        saveConfig();
                    }
                }
            }
            if (event.getInventory().getName().equals(ChatColor.DARK_GRAY + "Island Settings")) {
                if (event.getCurrentItem().getType().equals(Material.STAINED_CLAY)) {
                    if (event.getInventory().getItem(19).getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Enabled")) {
                        createItem(playerSettings, 19, Material.STAINED_CLAY, 14, ChatColor.RED + "Disabled", new String[]{ChatColor.DARK_GRAY + "Click to enable!"});
                        Bukkit.getWorld(event.getWhoClicked().getWorld().getName()).playSound(event.getWhoClicked().getLocation(), Sound.LEVEL_UP, 5, 5);
                        getConfig().set("Users." + event.getWhoClicked().getUniqueId().toString() + ".Settings.Island.Visitable", Boolean.valueOf(false));
                        saveConfig();
                    } else if (event.getInventory().getItem(19).getItemMeta().getDisplayName().equals(ChatColor.RED + "Disabled")) {
                        createItem(playerSettings, 19, Material.STAINED_CLAY, 5, ChatColor.GREEN + "Enabled", new String[]{ChatColor.DARK_GRAY + "Click to disable!"});
                        Bukkit.getWorld(event.getWhoClicked().getWorld().getName()).playSound(event.getWhoClicked().getLocation(), Sound.LEVEL_UP, 5, 5);
                        getConfig().set("Users." + event.getWhoClicked().getUniqueId().toString() + ".Settings.Island.Visitable", Boolean.valueOf(true));
                        saveConfig();
                    }
                }
            }
        }
    }

    @EventHandler
    public void interact(PlayerInteractEvent event){
        if (!event.getAction().toString().toLowerCase().contains("right") || event.getItem() == null) return;
        if ((event.getItem().getType().equals(Material.NETHER_STAR))){
            if (event.getItem().getItemMeta().getDisplayName().equals(ChatColor.GOLD + "Options Menu")){
                openMainMenu(event.getPlayer());
            }

        }
    }

    public void createItem(Inventory inv, int slot, Material item, int dataint, String displayName, String[] lores) {
        ItemStack i = new ItemStack(item, 1, (byte) dataint);
        ItemMeta m = i.getItemMeta();
        m.setDisplayName(displayName);
        m.setLore(Arrays.asList(lores));
        i.setItemMeta(m);
        inv.setItem(slot, i);
    }

    public void openMainMenu(Player player){
        Inventory inventory = Bukkit.createInventory(null, 9 * 3, ChatColor.DARK_GRAY + "Main menu");
        createItem(inventory, 11, Material.SAPLING, 0, ChatColor.AQUA + "Invite players", new String[]{ChatColor.DARK_GRAY + "Use this to invite players to", ChatColor.DARK_GRAY + "your island!"});
        createItem(inventory, 13, Material.GRASS, 0, ChatColor.AQUA + "Visit islands", new String[]{ChatColor.DARK_GRAY + "Visit players islands who are online!"});
        createItem(inventory, 15, Material.REDSTONE_COMPARATOR, 0, ChatColor.AQUA + "Settings", new String[]{ChatColor.DARK_GRAY + "This opens the settings menu", ChatColor.DARK_GRAY + "for your island and you!"});
        createItem(inventory, 26, Material.ARROW, 0, ChatColor.RED + "Exit", new String[]{ChatColor.DARK_GRAY + "Click to exit menu!"});
        player.openInventory(inventory);
    }

    public void openSettingsMenu(Player player){
        Inventory inventory = Bukkit.createInventory(null, 9 * 3, ChatColor.DARK_GRAY + "Settings");
        createItem(inventory, 12, Material.GRASS, 0, ChatColor.AQUA + "Island Settings", new String[]{ChatColor.DARK_GRAY + "This opens yours islands settings!"});
        createItem(inventory, 14, Material.REDSTONE, 0, ChatColor.AQUA + "Your Settings", new String[]{ChatColor.DARK_GRAY + "This opens your settings!"});
        player.openInventory(inventory);
    }

    public void openPlayerSettingsMenu(Player player){
        playerSettings = Bukkit.createInventory(player, 9 * 4, ChatColor.DARK_GRAY + player.getName() + "'s Settings");
        createItem(playerSettings, 10, Material.PAPER, 0, ChatColor.AQUA + "Invite notifications", new String[]{ChatColor.DARK_GRAY + "Click to disable or enable the", ChatColor.DARK_GRAY + "the invite notifications!"});
        createItem(playerSettings, 12, Material.BOOK, 0, ChatColor.AQUA + "Have an idea?", new  String[]{ChatColor.DARK_GRAY + "At the moment we don't know what", ChatColor.DARK_GRAY + "to add here..."});
        createItem(playerSettings, 14, Material.BOOK_AND_QUILL, 0, ChatColor.AQUA + "Have an idea?", new  String[]{ChatColor.DARK_GRAY + "At the moment we don't know what", ChatColor.DARK_GRAY + "to add here..."});
        createItem(playerSettings, 16, Material.BOOKSHELF, 0, ChatColor.AQUA + "Have an idea?", new  String[]{ChatColor.DARK_GRAY + "At the moment we don't know what", ChatColor.DARK_GRAY + "to add here..."});
        if (getConfig().getBoolean("Users." + player.getUniqueId().toString() + ".Settings.Players.InviteNotifications")){
            createItem(playerSettings, 19, Material.STAINED_CLAY, 5, ChatColor.GREEN + "Enabled", new String[]{ChatColor.DARK_GRAY + "Click to disable!"});
        } else {
            createItem(playerSettings, 19, Material.STAINED_CLAY, 14, ChatColor.RED + "Disabled", new String[]{ChatColor.DARK_GRAY + "Click to enable!"});
        }
        player.openInventory(playerSettings);
    }

    public void openIslandSettingsMenu(Player player){
        islandSettings = Bukkit.createInventory(player, 9*4, ChatColor.DARK_GRAY + "Island Settings");
        createItem(islandSettings, 10, Material.BOAT, 0, ChatColor.AQUA + "Visitable", new String[]{ChatColor.DARK_GRAY + "Click to disable or enable the", ChatColor.DARK_GRAY + "ability for players to visit", ChatColor.DARK_GRAY + "your island!"});
        if (getConfig().getBoolean("Users." + player.getUniqueId().toString() + ".Settings.Island.Visitable")){
            createItem(playerSettings, 19, Material.STAINED_CLAY, 5, ChatColor.GREEN + "Enabled", new String[]{ChatColor.DARK_GRAY + "Click to disable!"});
        } else {
            createItem(playerSettings, 19, Material.STAINED_CLAY, 14, ChatColor.RED + "Disabled", new String[]{ChatColor.DARK_GRAY + "Click to enable!"});
        }
    }
}
